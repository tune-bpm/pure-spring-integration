package net.pervukhin.purespringintegration.integrationcomponent;

import net.pervukhin.purespringintegration.service.RestExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component("restServiceActivator")
public class RestServiceActivator {
    private static final Logger logger = LoggerFactory.getLogger(RestServiceActivator.class);

    @Autowired
    private RestExecutorService restExecutorService;

    @ServiceActivator
    public Message<Integer> restA(Message<Integer> message) {
        restExecutorService.doRestRequest();
        return message;
    }

    @ServiceActivator
    public Message<Integer> restB(Message<Integer> message) {
        restExecutorService.doRestRequest();
        logger.debug("Исходное сообщение:" + message.getPayload());
        return message;
    }
}
