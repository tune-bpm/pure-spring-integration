package net.pervukhin.purespringintegration.integrationcomponent;

public interface StarterGateway {
    void start(Integer payload);
}
