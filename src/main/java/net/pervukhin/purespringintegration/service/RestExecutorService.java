package net.pervukhin.purespringintegration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@Service
public class RestExecutorService {
    private static final Logger logger = LoggerFactory.getLogger(RestExecutorService.class);
    private static final Random random = new Random();

    @Autowired
    private RestTemplate restTemplate;

    @Value("${rest.request.url}")
    private String restRequestUrl;

    public void doRestRequest() {
        final String result = restTemplate.getForObject(restRequestUrl + "?v=" + random.nextLong(), String.class);
        logger.debug(result);
    }
}
