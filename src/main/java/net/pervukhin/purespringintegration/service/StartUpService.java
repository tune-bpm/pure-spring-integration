package net.pervukhin.purespringintegration.service;

import net.pervukhin.purespringintegration.integrationcomponent.StarterGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class StartUpService {
    private static final Logger logger = LoggerFactory.getLogger(StartUpService.class);

    @Autowired
    private StarterGateway starterGateway;

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        logger.info("На старт");
        load(1000);
        load(10000);
        load(20000);
        load(30000);
        load(40000);
        load(50000);
        load(60000);
        load(70000);
        load(80000);
        load(90000);
        load(100000);
    }

    private void load(int quantity) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < quantity; i++) {
            starterGateway.start(i);
        }
        long endTime = System.currentTimeMillis();
        logger.info("Время выполнения|"+ quantity +"|" + (endTime-startTime));
    }
}
