package net.pervukhin.purespringintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PureSpringIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(PureSpringIntegrationApplication.class, args);
    }

}
